<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
    <html>
        <link rel="stylesheet" type="text/css" href="custom.css"/>
        <body>
            <h1 style="text-align: center; text-decoration: underline">All Employees</h1>
            <table border="1">
                <tr>
                    <th width="110">ID</th>
                    <th width="200">Name</th>
                    <th>Date of Birth</th>
                    <th width="200">Phone</th>
                    <th width="150">Salary</th>
                </tr>
                <xsl:for-each select="employees/employee">
                    <xsl:sort select="salary" data-type="number" order="descending"/>
                    <tr>
                        <!--<xsl:if test="starts-with(name, 'L')">-->
                        <!--<xsl:if test="salary &gt; 1000">-->

                            <td><xsl:value-of select="@id"></xsl:value-of></td>
                            <td class="name"><xsl:value-of select="name"></xsl:value-of></td>
                            <td class="dob"><xsl:value-of select="dob"></xsl:value-of></td>
                            <td class="phone"><xsl:value-of select="phone"></xsl:value-of></td>
                            <td class="salary"><xsl:value-of select="salary"></xsl:value-of></td>

                    </tr>
                </xsl:for-each>
            </table>

            <h1><center>Search By Phone and Year</center></h1>
            <table border="1">
                <tr>
                    <th width="110">ID</th>
                    <th width="200">Name</th>
                    <th>Date of Birth</th>
                    <th width="200">Phone</th>
                    <th width="150">Salary</th>
                </tr>
                <xsl:for-each select="employees/employee">
                    <xsl:sort select="salary" data-type="number" order="descending"/>
                    <tr>
                        <!--<xsl:if test="starts-with(name, 'L')">-->
                        <!--<xsl:if test="salary &gt; 1000">-->
                        <xsl:if test="starts-with(phone, '012') and substring(dob, 7, 4) = '1999'">
                            <td><xsl:value-of select="@id"></xsl:value-of></td>
                            <td class="name"><xsl:value-of select="name"></xsl:value-of></td>
                            <td class="dob"><xsl:value-of select="dob"></xsl:value-of></td>
                            <td class="phone"><xsl:value-of select="phone"></xsl:value-of></td>
                            <td class="salary"><xsl:value-of select="salary"></xsl:value-of></td>
                        </xsl:if>
                    </tr>
                </xsl:for-each>
            </table>
        </body>
    </html>

</xsl:template>
</xsl:stylesheet>